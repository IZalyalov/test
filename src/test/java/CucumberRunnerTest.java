import com.codeborne.selenide.Configuration;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "stepdefs",
        plugin = "io.qameta.allure.cucumber2jvm.AllureCucumber2Jvm",
        junit = "--step-notifications",
        format = "pretty",
        tags = "@test"
)

public class CucumberRunnerTest {

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        Configuration.browser = "chrome";
        Configuration.baseUrl = "http://vk.com/";
        Configuration.startMaximized = true;
        Configuration.screenshots = false;
        Configuration.savePageSource = false;
        Configuration.fastSetValue = false;
        Configuration.reportsFolder = "target/selenide";
    }
}