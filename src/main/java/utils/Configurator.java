package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configurator {

    static private Properties properties = new Properties();

    static {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src/test/resources/configs/config.properties");
            properties.load(fis);
        } catch (IOException e) {
            throw new ExceptionInInitializerError("Не найден файл .properties");
        }
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }
}