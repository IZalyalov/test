package annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PageEntry {
    /**
     * Название страницы
     */
    String title();
//
//    /**
//     * Указывается iframe, в котором находятся элементы страницы, по-умолчанию - активный фрейм
//     */
//    Frames frame() default Frames.STANDART_ACTIVE_FRAME;
}