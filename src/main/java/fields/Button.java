package fields;

import com.codeborne.selenide.SelenideElement;

public class Button extends AbstractField {

    public Button(String name, SelenideElement selenideElement) {
        super(name, selenideElement);
    }
}
