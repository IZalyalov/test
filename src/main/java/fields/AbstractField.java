package fields;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

public abstract class AbstractField {
    protected String name;
    protected SelenideElement selenideElement;

    private static final String NORMALIZE_SPACE_XPATH = "./descendant-or-self::*[normalize-space(translate(text(),'\u00A0',' '))='%s']";

    public AbstractField(String name, SelenideElement selenideElement) {
        this.name = name;
        this.selenideElement = selenideElement;
    }

    public void click() {
        selenideElement.shouldBe(Condition.enabled).click();
    }

    public boolean isDisplayed() {
        return selenideElement.isDisplayed();
    }

    public boolean isEnabled() {
        return selenideElement.isEnabled();
    }

    public void fillUp(String s) {
        throw new IllegalStateException("Невозможно заполнить поле! " + this);
    }

    public SelenideElement getSelenideElement() {
        return selenideElement;
    }

    public String getText() {
        return selenideElement.getText();
    }

    public String getValue() {
        return selenideElement.getValue();
    }

    public void clear() {
        selenideElement.clear();
    }

    public boolean containsText(String text) {
        return selenideElement.$x(String.format(NORMALIZE_SPACE_XPATH, text)).isDisplayed();
    }

    @Override
    public String toString() {
        return String.format("type: [%s]; name: [%s]", this.getClass().getCanonicalName(), name);
    }
}
