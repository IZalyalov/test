package fields;

import com.codeborne.selenide.SelenideElement;

public class Input extends AbstractField {

    public Input(String name, SelenideElement selenideElement) {
        super(name, selenideElement);
    }

    @Override
    public void fillUp(String s) {
        clear();
        selenideElement.sendKeys(s);
    }

    @Override
    public String getText() {
        return getValue();
    }
}