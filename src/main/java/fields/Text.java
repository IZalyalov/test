package fields;

import com.codeborne.selenide.SelenideElement;

public class Text extends AbstractField {

    public Text(String name, SelenideElement selenideElement) {
        super(name, selenideElement);
    }

    @Override
    public String getValue() {
        return getText();
    }
}
