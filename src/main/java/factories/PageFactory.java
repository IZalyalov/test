package factories;

import java.util.HashMap;
import java.util.Map;

import annotations.PageEntry;
import exceptions.AutotestError;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.AbstractPage;

public abstract class PageFactory {

    private static final Logger logger = LoggerFactory.getLogger(PageFactory.class);
    private static final Map<String, Class<? extends AbstractPage>> PAGES_CACHE = new HashMap<>();

    public static AbstractPage getPage(String name) {
        Class<? extends AbstractPage> pageClass = PAGES_CACHE.get(name);
        if (pageClass == null) throw new NullPointerException(String.format("Страница [%s] не найдена", name));
        return construct(pageClass);
    }

    private static AbstractPage construct(Class<? extends AbstractPage> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new AutotestError(String.format("Ошибка создания экземпляра страницы [%s]", clazz));
        }
    }

    static {
        Reflections reflections = new Reflections("pages");
        reflections.getSubTypesOf(AbstractPage.class).forEach(clazz -> {
            PageEntry pageEntry = clazz.getAnnotation(PageEntry.class);
            if (pageEntry == null)
                logger.warn(String.format("В классе [%s] не найдана аннотация @PageEntry", clazz.getCanonicalName()));
            else PAGES_CACHE.put(pageEntry.title(), clazz);
        });
    }
}
