package factories;

import annotations.ElementTitle;
import annotations.PageLoadMark;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import exceptions.AutotestError;
import fields.AbstractField;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.AbstractPage;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;


public abstract class FieldFactory {

    private static final Logger logger = LoggerFactory.getLogger(FieldFactory.class);

    public static AbstractField getField(String title, Class<? extends AbstractPage> page) {
        return getAbstractField(findField(title, page));
    }

    public static List<AbstractField> getFields(String title, Class<? extends AbstractPage> page) {
        return getAbstractFields(findField(title, page));
    }

    private static AbstractField getAbstractField(Field field) {
        SelenideElement selenideElement = $x(field.getAnnotation(FindBy.class).xpath());
        try {
            return (AbstractField) field.getType().getConstructor(String.class, SelenideElement.class).newInstance(field.getAnnotation(ElementTitle.class).value(), selenideElement);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new AutotestError("Ошибка создания экземляра AbstractField");
        }
    }

    private static List<AbstractField> getAbstractFields(Field field) {
        ElementsCollection elementsCollection = $$x(field.getAnnotation(FindBy.class).xpath());
        return elementsCollection.stream().map(selenideElement -> {
            try {
                return (AbstractField) field.getType().getConstructor(String.class, SelenideElement.class)
                        .newInstance(field.getAnnotation(ElementTitle.class).value(), selenideElement);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new AutotestError("Ошибка создания экземляра AbstractField");
            }
        }).collect(Collectors.toList());
    }

    private static Field findField(String title, Class<? extends AbstractPage> page) {
        return Arrays.stream(page.getDeclaredFields()).filter(field -> field.getAnnotation(ElementTitle.class).value().equals(title))
                .findFirst().orElseThrow(() -> new NotFoundException(String.format("Не найдено поле [%s]", title)));
    }

    public static List<AbstractField> getPageLoadMarks(Class<? extends AbstractPage> page) {
        return Arrays.stream(page.getDeclaredFields())
                .filter(f -> f.getAnnotation(PageLoadMark.class) != null)
                .map(FieldFactory::getAbstractField).collect(Collectors.toList());
    }
}
