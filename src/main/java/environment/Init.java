package environment;

import exceptions.AutotestError;
import pages.AbstractPage;

public class Init {
    private static AbstractPage currentPage;

    public static void setCurrentPage(AbstractPage abstractPage) {
        currentPage = abstractPage;
    }

    public static AbstractPage getCurrentPage() {
        if (currentPage == null) throw new AutotestError("Страница не инициализирована");
        return currentPage;
    }

    public static void reset() {
        currentPage = null;
    }

}
