package stepdefs;

import environment.Init;
import exceptions.AutotestError;
import factories.PageFactory;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.ru.И;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.AbstractPage;
import utils.ActionWait;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class PageSteps {
    Logger logger = LoggerFactory.getLogger(PageSteps.class);

    @И("^пользователь переходит на главную страницу$")
    @Step
    public void goToLoginPage() {
        open("");
    }

    @И("^открывается \"([^\"]*)\"$")
    @Step
    public void pageOpened(String pageTitle) {
        AbstractPage page = PageFactory.getPage(pageTitle);
        if (!ActionWait.shortTimeout().safeCall(webDriver -> page.isOpened()))
            throw new AutotestError(String.format("Не открылась [%s]", pageTitle));
        Init.setCurrentPage(page);
    }

    @И("^завершается текущий сеанс$")
    @Step
    public static void endSession() {
        SelenideElement topProfileLink = $x("//a[@id='top_profile_link']");
        if (topProfileLink.isDisplayed()) {
            topProfileLink.click();
            $(byText("Выйти")).shouldBe(Condition.visible).click();
        }
    }
}
