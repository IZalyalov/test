package stepdefs;

import environment.Init;
import cucumber.api.java.ru.И;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FieldSteps {
    Logger logger = LoggerFactory.getLogger(FieldSteps.class);

    @И("^поле \"([^\"]*)\" заполняется значением \"([^\"]*)\"$")
    @Step
    public void fillUpField(String elementTitle, String value) {
        Init.getCurrentPage().getField(elementTitle).fillUp(value);
    }

    @И("^нажимается (?:поле|кнопка) \"([^\"]*)\"$")
    @Step
    public void clickField(String elementTitle) {
        Init.getCurrentPage().getField(elementTitle).click();
    }
}
