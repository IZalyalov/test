package stepdefs;

import environment.Init;
import cucumber.api.DataTable;
import cucumber.api.java.ru.И;
import exceptions.AutotestError;
import fields.AbstractField;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListSteps {
    Logger logger = LoggerFactory.getLogger(ListSteps.class);

    private AbstractField findElementByText(String listTitle, String text) {
        return Init.getCurrentPage().getFields(listTitle).stream()
                .filter((field) -> field.containsText(text)).findFirst()
                .orElseThrow(() -> new AutotestError(String.format("В списке [%s] не найден элемент с текстом [%s]", listTitle, text)));
    }

    @И("^в списке \"([^\"]*)\" нажимается \"([^\"]*)\"$")
    @Step
    public void clickListField(String listTitle, String elementText) {
        findElementByText(listTitle, elementText).click();
    }

    @И("^список \"([^\"]*)\" содержит поле \"([^\"]*)\"$")
    @Step
    public void listContainsElement(String listTitle, String elementText) {
        Assert.assertTrue(String.format("Список [%s] не содержит поле [%s]", listTitle, elementText),
                findElementByText(listTitle, elementText).isDisplayed());
    }

    @И("^список \"([^\"]*)\" содержит поля:$")
    @Step
    public void listContainsElements(String listTitle, DataTable dataTable) {
        dataTable.asLists(String.class).forEach(row -> listContainsElement(listTitle, row.get(0)));
    }
}
