package stepdefs;

import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import environment.Init;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static utils.Utils.attachScreenshot;

public class Hooks {
    Logger logger = LoggerFactory.getLogger(Hooks.class);

    @Before()
    public void reset() {
        Init.reset();
    }

    @After(order = 1)
    public void endSession() {
        if (WebDriverRunner.hasWebDriverStarted())
            PageSteps.endSession();
    }

    @After(order = 2)
    public void attachScreenshotIfScenarioFiled(Scenario scenario) {
        if (scenario.isFailed() && WebDriverRunner.hasWebDriverStarted()) {
            attachScreenshot();
        }
    }
}