package stepdefs;

import cucumber.api.java.ru.И;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import static utils.Utils.attachScreenshot;

public class UtilSteps {
    Logger logger = LoggerFactory.getLogger(UtilSteps.class);

    @И("^сохраняется скриншот страницы$")
    public static void saveScreenshot() {
        attachScreenshot();
    }
}
