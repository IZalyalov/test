package pages;

import annotations.ElementTitle;
import annotations.PageEntry;
import annotations.PageLoadMark;
import fields.Text;
import org.openqa.selenium.support.FindBy;

@PageEntry(title = "Страница Сообщения")
public class MessagesPage extends AbstractPage {

    @ElementTitle("Все сообщения")
    @PageLoadMark
    @FindBy(xpath = "//span[text()='Все сообщения']")
    private Text reg;

    @ElementTitle("Меню")
    @FindBy(xpath = "//div[@id='side_bar_inner']//li")
    private Text menu;
}
