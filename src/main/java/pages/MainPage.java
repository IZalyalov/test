package pages;

import annotations.ElementTitle;
import annotations.PageEntry;
import annotations.PageLoadMark;
import fields.Button;
import fields.Input;
import fields.Text;
import org.openqa.selenium.support.FindBy;

@PageEntry(title = "Главная страница")
public class MainPage extends AbstractPage {

    @ElementTitle("Телефон или email")
    @FindBy(xpath = "//input[@placeholder='Телефон или email']")
    private Input login;

    @ElementTitle("Пароль")
    @FindBy(xpath = "//input[@placeholder='Пароль']")
    private Input pass;

    @ElementTitle("Моментальная регистрация")
    @PageLoadMark
    @FindBy(xpath = "//div[@class='page_block ij_form']")
    private Text reg;

    @ElementTitle("Войти")
    @FindBy(xpath = "//button[text()='Войти' and not(ancestor::*[contains(@style,'none')])]")
    private Button enter;
}
