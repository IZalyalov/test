package pages;

import factories.FieldFactory;
import fields.AbstractField;

import java.util.List;

public abstract class AbstractPage {

    public boolean isOpened() {
        return FieldFactory.getPageLoadMarks(this.getClass()).stream().allMatch(AbstractField::isDisplayed);
    }

    public AbstractField getField(String elementTitle) {
        return FieldFactory.getField(elementTitle, this.getClass());
    }

    public List<AbstractField> getFields(String collectionTitle) {
        return FieldFactory.getFields(collectionTitle, this.getClass());
    }
}