package pages;

import annotations.ElementTitle;
import annotations.PageEntry;
import annotations.PageLoadMark;
import fields.Text;
import org.openqa.selenium.support.FindBy;

@PageEntry(title = "Страница Новости")
public class FeedPage extends AbstractPage {

    @ElementTitle("Что у Вас нового?")
    @PageLoadMark
    @FindBy(xpath = "//div[text()='Что у Вас нового?']")
    private Text reg;

    @ElementTitle("Меню")
    @FindBy(xpath = "//div[@id='side_bar_inner']//li")
    private Text menu;
}
